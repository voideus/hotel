@extends('layouts.app')

@section('content')


<div class="container">
<div class="container mt-5">
    
        <h2 style="display: inline;">{{ $business->business_name }}</h2>
        <star-rating :read-only="true" :rating={{$business->getStarRating()}} :star-size="20"></star-rating>
</div>

<div class="content mx-3">
       <p> {{$business->b_description}}</p>
</div>

 {{-- adding reviews --}}
<div class="row my-3">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">Write a Review</div>

            <div class="card-body">

                <review-form 
                :business = "{{$business}}"
                url = "/business/{business}/reviews"
                >

                </review-form>
                    
                    {{-- <form method="POST" action="/business/{{$business->id}}/reviews"> --}}
                        {{-- <form method="POST" action="/business/{{$business->id}}/reviews">
                      
                       
                            @csrf
                            <div class="form-group row">
                                <label for="rating" class="col-md-4 col-form-label text-md-right">Ratings</label>
    
                                <div class="col-md-6">
                                   <input name="rating" type="text">
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Review</label>
    
                                <div class="col-md-6">
                                    <textarea name="description" id="" cols="30" rows="7" placeholder="" required></textarea>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit"  class="button" >Submit</button>
                                </div>
                            </div>
                        </form> --}}
                        
                </div>

            </div>
        </div>
</div>
<a href="#img">user uploaded images</a>
<h3>Reviews</h3>
<section class="light-bg booking-details_wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-8 responsive-wrap">
        @if ($business->reviews->count()>0)
            <ul>
                @foreach ($business->reviews as $review)
                    {{-- <div class="content">
                        <h4>{{ $review->user->name}}</h4>
                        <li>{{ $review->description }}</li>
                    </div> --}}
                    <div class="customer-review_wrap">
                        <div class="customer-img">
                            
                            <p>{{ $review->user->name}}</p>
                            
                        </div>
                    <div class="customer-content-wrap">
                        <div class="customer-content">
                                <star-rating :read-only="true" :rating={{$review->rating}} :star-size="20" :show-rating="false"></star-rating>
                            <div class="customer-rating">{{$review->rating}}</div>
                        </div>
                        <p class="customer-text">
                            {{ $review->description }}
                        </p>                   
                    </div>
                </div>
                <hr>
                @endforeach
            </ul>
            
        @endif
            </div>
        </div>
    </div>
</section>
</div>
</div>

 <div id="img" class="container-fluid">
        <!-- Swiper -->
        <div class="swiper-container">
            <div class="swiper-wrapper">

                <div class="swiper-slide">
                    <a href="images/reserve-slide2.jpg" class="grid image-link">
                        <img src="/images/reserve-slide2.jpg" class="img-fluid" alt="#">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="/images/reserve-slide1.jpg" class="grid image-link">
                        <img src="/images/reserve-slide1.jpg" class="img-fluid" alt="#">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="/images/reserve-slide3.jpg" class="grid image-link">
                        <img src="/images/reserve-slide3.jpg" class="img-fluid" alt="#">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="/images/reserve-slide1.jpg" class="grid image-link">
                        <img src="/images/reserve-slide1.jpg" class="img-fluid" alt="#">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="/images/reserve-slide2.jpg" class="grid image-link">
                        <img src="/images/reserve-slide2.jpg" class="img-fluid" alt="#">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="/images/reserve-slide3.jpg" class="grid image-link">
                        <img src="/images/reserve-slide3.jpg" class="img-fluid" alt="#">
                    </a>
                </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination swiper-pagination-white"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>
        </div>
    </div>


   
   <!-- Swipper Slider JS -->
    <script type="text/javascript" src="{{ URL::asset('js/swiper.min.js') }}"></script>
    {{-- <script src="js/swiper.min.js"></script> --}}
    <script>
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 3,
            slidesPerGroup: 3,
            loop: true,
            loopFillGroupWithBlank: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    </script>


@endsection
