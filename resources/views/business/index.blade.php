{{-- @extends('layouts.app')

@section('content')

<div class="container">
    <h2 class="mt-5">All Bussiness Page</h2>
    
    <ul>
        <div>
            @foreach ($businesses as $business)

            <li>
                <a href="/business/{{$business->id}}">
                    {{$business->business_name}}
                    {{$business->getStarRating()}}
                </a>
            </li>
                
            @endforeach
        </div>
    </ul>
</div>


@endsection --}}


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
     <link rel="dns-prefetch" href="//fonts.gstatic.com">
     <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
 
     <!-- Styles -->
     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow-sm fixed-top container-fluid d-flex">
                 <div class="container">
                     <a class="navbar-brand" href="{{ url('/') }}">
                         {{-- {{ config('app.name', 'Laravel') }} --}}Trip Advisor
                     </a>
                     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                         <span class="navbar-toggler-icon"></span>
                     </button>
     
                     <div class="collapse navbar-collapse" id="navbarSupportedContent">
                         <!-- Left Side Of Navbar -->
                         <ul class="navbar-nav mr-auto">
     
                         </ul>
     
                         <!-- Right Side Of Navbar -->
                         <ul class="navbar-nav ml-auto">
                             <!-- Authentication Links -->
                             @guest
                             <li class="nav-item">
                                     <a class="nav-link" href="{{ route('login') }}">Home</a>
                                 </li>
                                 <li class="nav-item">
                                         <a class="nav-link" href="{{ route('login') }}">About</a>
                                     </li>
                                     <li class="nav-item">
                                             <a class="nav-link" href="{{ route('login') }}">Our team</a>
                                         </li>
                                 <li class="nav-item">
                                     <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                 </li>
                                 @if (Route::has('register'))
                                     <li class="nav-item">
                                         <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                     </li>
                                 @endif
                             @else
                             <li class="nav-item">
                                     <a class="nav-link" href="/home">Home</a>
                             </li>
                             <li class="nav-item">
                                     <a class="nav-link" href="/business">Business</a>
                             </li>
                             <li class="nav-item">
                                         <a class="nav-link" href="/about">About</a>
                                     </li>
                                     <li class="nav-item">
                                             <a class="nav-link" href="/ourteam">Our team</a>
                                         </li>
                                 <li class="nav-item dropdown">
                                     <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                         {{ Auth::user()->name }} <span class="caret"></span>
                                     </a>
     
                                     <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                         <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                          document.getElementById('logout-form').submit();">
                                             {{ __('Logout') }}
                                         </a>
     
                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                             @csrf
                                         </form>
                                     </div>
                                 </li>
                             @endguest
                         </ul>
                     </div>
                    </nav>
         </div>
    <div class="title" align="center">
        <h1>HOTELS AVAILABLE</h1>
        <p>Yoga center organizes various Yoga classes.</p>
</div>
<br/>

<div class="container">
    <div class="row">
        <div class="col-11">
            <div class="heading">
               <h3 align="center"><b>WE FOUND 15 AVAILABLE YOGA CLASSES FOR YOU</b></h3>
            </div>
            <br/>
            @foreach ($businesses as $business)
                <div class="card mb-5" style="margin-left:50px">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="{{ asset('business/') }}" style="margin:10px" width="75px" height="350px" class="card-img" alt="jhk">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                    
                                <a href="/business/{{$business->id}}">
                                    {{$business->business_name}}
                                    {{$business->getStarRating()}}
                                </a>
                                <hr>
                                <h5 class="card-title"><b>{{$business->business_name}}</b></h5>
                                
                                <p class="card-text-description" style="font-size: 15px" >{{str_limit($business->description , 200 )}}</p>                             
                               

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

           
        </div>
    </div>
</div>
        <div id="map" style="height: 400px; width:60%;"></div>

        {{-- <script type="text/javascript">

            var users = {!! json_encode($businesses->toArray()) !!};
        
         
        
            console.log(users);
        
        </script> --}}
        <script>

            function initMap() {
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: {lat: 27.7172, lng: 85.3240}
          });
        
          setMarkers(map);
        }
        
        var beaches = [
        ['Kalanki Pharmacy',27.6931,85.2807,15],
        ['Satdobato Pharmacy',27.6515,85.3278,14],
        ['Gwarko pharmacy',27.6663,85.333,13],
        ['Balkumari Pharmacy',27.6695,85.3408,12],
        ['Koteswoar Pharmacy',27.6756,85.3459,11],
        ['Tinkune Pharmacy',27.6854,85.3489,10],
        ['Airport Pharmacy',27.6981,85.3592,9],
        ];
        var users = {!! json_encode($businesses->toArray()) !!};
        
        function setMarkers(map) {
          var shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
          };
          for(var i = 0; i < users.length; i++){
              console.log(users[i].lat , users[i].lng);
              var marker = new google.maps.Marker({
              position: {lat: users[i].lat, lng: users[i].lng},
              map: map,
            });

          }
        //   for (var i = 0; i < beaches.length; i++) {
        //     var beach = beaches[i];
        //     var marker = new google.maps.Marker({
        //       position: {lat: beach[1], lng: beach[2]},
        //       map: map,
        //     });
        //   }
        }
        
              
         
            </script>
            <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsFY34O6QH3hM-_mbxRvdIqwXg1-CyeXQ&callback=initMap">
            </script>
</body>
</html>








{{-- @extends('layouts.app')

@section('content')
    <div class="title" align="center">
            <h1>HOTELS AVAILABLE</h1>
            <p>Yoga center organizes various Yoga classes.</p>
    </div>
    <br/>

    <div class="container">
        <div class="row">
            <div class="col-11">
                <div class="heading">
                   <h3 align="center"><b>WE FOUND 15 AVAILABLE YOGA CLASSES FOR YOU</b></h3>
                </div>
                <br/>
                @foreach ($businesses as $business)
                    <div class="card mb-5" style="margin-left:50px">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img src="{{ asset('business/') }}" style="margin:10px" width="75px" height="350px" class="card-img" alt="jhk">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                        
                                    <a href="/business/{{$business->id}}">
                                        {{$business->business_name}}
                                        {{$business->getStarRating()}}
                                    </a>
                                    <hr>
                                    <h5 class="card-title"><b>{{$business->business_name}}</b></h5>
                                    
                                    <p class="card-text-description" style="font-size: 15px" >{{str_limit($business->description , 200 )}}</p>                             
                                   

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

               
            </div>
        </div>
    </div>

@endsection  --}}