@extends('layouts.app')

@section('content')
<div class=" container header mt-5">

    <h4 style= "padding:30px;">About Us</h4>
    <p>Hotel Advisor is a hotel directory for users to discover hotels around them and according to their needs.It also faciliates the users to review the hotels that 
    they have previously visited. Our web application helps the local hotels to promote their business and get feedback from customers through reviews so that businesses can reach out to more number of people. 
    </p>
</div>

@endsection 