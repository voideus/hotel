<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $guarded = [
    
    ];

    public function reviews() {
       return  $this->hasMany(Review::class);
    }
    

    public function getStarRating()
    {
        $count = $this->reviews()->count();
        if(empty($count)){
            return 0;
        }
        $starCountSum=$this->reviews()->sum('rating');
        $average=$starCountSum/ $count;

       return $average;

    }

    // public function addTask($rating){
    //     dd($rating);
    // }
}
