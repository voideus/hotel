<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $guarded = [
    ];

    public function business() {
        return $this->belongsTo(Business::class);
    }

    public function user() {
       return  $this->belongsTo(User::class,'owner_id');
    }
}
