<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/about', function () {
    return view('aboutus');
});
Route::get('/ourteam', function () {
    return view('ourteam');
});
// Route::get('/business', function () {
//     return view('business.show');
// });

Route::resource('business', 'BusinessController');

Route::post('/business/{business}/reviews', 'ReviewsController@store');

// Route::resource('review', 'ReviewsController');